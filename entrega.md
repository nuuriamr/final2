# ENTREGA CONVOCATORIA JUNIO

Nuria Mordillo Ramírez\
n.mordillo.2023@alumnos.urjc.es\
Enlace Youtube:https://youtu.be/O1VcWt7Q1ZY?si=iwuwx6vHZZYMHeHd


Funciones usadas en el proyecto final convocatoria de Junio 2024:

Funciones del requisito mínimo:
- MIRROR
- GRAYSCALE
- BLUR
- CHANGE_COLORS
- ROTATE
- SHIFT
- CROP
- FILTER

Funciones del requisito opcional:
- SEPIA
- NEGATIVE
- LUMINITY