#from transforms import mirror,grayscale
from images import read_img, write_img
import sys
from transforms import *
def parametro():
    image_filename = sys.argv[1]
    transforms_name = sys.argv[2]
    return image_filename, transforms_name


def main():
    archivo,transforms=parametro()
    image=read_img(archivo)

    if transforms == "mirror":
        image = mirror(image)
    elif transforms == "grayscale":
        image = grayscale(image)
    elif transforms == "blur":
        image = blur(image)
    elif transforms == "sepia":
        image = sepia(image)
    elif transforms == "negative":
        image = negative(image)
    elif transforms=="luminity":
        parametro0=int(sys.argv[3])
        image=luminity(image,parametro0)
    elif transforms == "change_colors":
        parametro1=sys.argv[3].split(":")
        parametro2=sys.argv[4].split(":")
        original=[]
        change=[]
        for i in range(len(parametro1)):

            r,g,b=parametro1[i].split(",")
            original.append((int(r),int(g),int(b)))

        for i in range(len(parametro2)):
            r, g, b = parametro2[i].split(",")
            change.append((int(r), int(g), int(b)))

        image =change_colors(image, original,change)
#rgb:rgb
    elif transforms =="rotate":
        parametro2 = sys.argv[3]
        image = rotate(image,parametro2)
#derecha o izquierda
    elif transforms == "shift":
        parametro3= int(sys.argv[3])
        parametro4 = int(sys.argv[4])
        image=shift(image,parametro3,parametro4)
#horixontal,vertical
    elif transforms == "crop":
        parametro5=int(sys.argv[3])
        parametro6 = int(sys.argv[4])
        parametro7 = int(sys.argv[5])
        parametro8 = int(sys.argv[6])
        image=crop(image,parametro5,parametro6,parametro7,parametro8)
#x,y,ancho y alto
    elif transforms=="filter":
        parametro9=float(sys.argv[3])
        parametro10 = float(sys.argv[4])
        parametro11 = float(sys.argv[5])
        image=filter(image,parametro9,parametro10,parametro11)
#parametro9=r,10=g,11=b
    else:
        print(f"Función no soportada: {transforms}")
        sys.exit(1)
    n=archivo.split("/")[-1].split(".")[0]
    partido = archivo.split(".")
    write_img(image, f"/home/alumnos/nuuriamr/final2/{n}_trans.jpg")

if __name__ == '__main__':
    main()
