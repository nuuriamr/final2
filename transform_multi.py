
import sys
from transforms import *
def parametro():
    image_filename = sys.argv[1]
    return image_filename


def main():

    archivo=parametro()
    image=read_img(archivo)
    n=2
    while True:

        transforms=sys.argv[n]
        n+=1
        if transforms == "mirror":
            image = mirror(image)
        elif transforms == "grayscale":
            image = grayscale(image)
        elif transforms == "blur":
            image = blur(image)
        elif transforms == "sepia":
            image = sepia(image)
        elif transforms == "negative":
            image = negative(image)
        elif transforms == "luminity":
            parametro0 = int(sys.argv[n])
            n+=1
            image = luminity(image, parametro0)
        elif transforms == "change_colors":
            parametro1=sys.argv[n].split(":")
            parametro2=sys.argv[n+1].split(":")
            original=[]
            change=[]
            n+=2
            for i in range(len(parametro1)):


                r,g,b=parametro1[i].split(",")
                original.append((int(r),int(g),int(b)))

            for i in range(len(parametro2)):
                r, g, b = parametro2[i].split(",")
                change.append((int(r), int(g), int(b)))
            image =change_colors(image, original,change)
#rgb:rgb
        elif transforms =="rotate":
            parametro2 = sys.argv[n]
            image = rotate(image,parametro2)
            n+=1
#derecha o izquierda
        elif transforms == "shift":
            parametro3= int(sys.argv[n])
            parametro4 = int(sys.argv[n+1])
            image=shift(image,parametro3,parametro4)
            n+=2
#horizontal y vertical
        elif transforms == "crop":
            parametro5=int(sys.argv[n])
            parametro6 = int(sys.argv[n+1])
            parametro7 = int(sys.argv[n+2])
            parametro8 = int(sys.argv[n+3])
            image=crop(image,parametro5,parametro6,parametro7,parametro8)
            n +=4
#x,y,horizontaly vertical
        elif transforms=="filter":
            parametro9=float(sys.argv[n])
            parametro10 = float(sys.argv[n+1])
            parametro11 = float(sys.argv[n+2])
            image=filter(image,parametro9,parametro10,parametro11)
            n+=3
#r*parametro9,g*10,b*11
        else:
            print(f"Función no soportada: {transforms}")
            sys.exit(1)

        if n ==len(sys.argv):
            break


    n = archivo.split("/")[-1].split(".")[0]
    write_img(image, f"/home/alumnos/nuuriamr/final2/{n}_trans.jpg")

if __name__ == '__main__':
    main()
