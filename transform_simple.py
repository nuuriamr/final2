#from transforms import mirror,grayscale
from images import read_img, write_img
import sys
from transforms import *
def parametro():

    if len(sys.argv) != 3:
        print("NO FUNCIONA REVISA EL COMANDO!")
        sys.exit(1)

    image_filename = sys.argv[1]
    transforms_name = sys.argv[2]
    return image_filename, transforms_name


def main():
    archivo,transforms=parametro()
    image=read_img(archivo)

    if transforms == "mirror":
        image = mirror(image)
    elif transforms == "grayscale":
        image = grayscale(image)
    elif transforms == "blur":
        image = blur(image)
    elif transforms == "sepia":
        image = sepia(image)
    elif transforms == "negative":
        image = negative(image)

    else:
        print(f"Función no soportada: {transforms}")
        sys.exit(1)
    n=archivo.split("/")[-1].split(".")[0]
    partido = archivo.split(".")
    write_img(image, f"/home/alumnos/nuuriamr/final2/{n}_trans.jpg")

if __name__ == '__main__':
    main()