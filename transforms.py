from images import *
def mirror(image: dict) -> dict:

    width, height = size(image)
    img = create_blank(width, height)

    for i in range(0, width*height):

        img["pixels"][i%width+width*(height-((i//width)+1))] = image["pixels"][i]

    return img
def grayscale(image: dict) -> dict:
    width, height = size(image)
    img = create_blank(width, height)
    for i in range(0,width*height):
        r=image["pixels"][i][0]
        g=image["pixels"][i][1]
        b=image["pixels"][i][2]

        color=(r + g + b)//3

        img["pixels"][i]=(color,color,color)

    return img

def blur (image: dict) -> dict:
    width, height = size(image)
    img = create_blank(width, height)
    for i in range(0,width*height):
        r, g, b = 0, 0, 0
        contador = 0
        if i//width !=0:
            r, g, b = image["pixels"][i-width][0]+r, image["pixels"][i-width][1]+g, image["pixels"][i-width][2]+b
            contador+=1
        if i//width != height-1:
            r, g, b = image["pixels"][i + width][0] + r, image["pixels"][i + width][1] + g, image["pixels"][i +width][2] + b
            contador += 1
        if i%width !=0:
            r, g, b = image["pixels"][i -1][0] + r, image["pixels"][i -1][1] + g, image["pixels"][i -1][2] + b
            contador += 1
        if i % width !=  width-1:
            r, g, b = image["pixels"][i + 1][0] + r, image["pixels"][i + 1][1] + g, image["pixels"][i + 1][2] + b
            contador += 1
        r,g,b =r//contador,g//contador,b//contador
        img["pixels"][i]=(r,g,b)
    return img

def change_colors(image: dict,
                  original: list[tuple[int, int, int]],
                  change: list[tuple[int, int, int]]) -> dict:
    width, height = size(image)
    img = create_blank(width, height)
    for i in range(0,width*height):
        img["pixels"][i]=image["pixels"][i]
        for j in range(len(original)):
            if img["pixels"][i]==original[j]:
                img["pixels"][i]=change[j]
                break
    return img

def rotate(image: dict, direction: str) -> dict:
    width, height = size(image)
    img = create_blank(height, width )


    for i in range(0, width * height):
        if direction == "right":
            img["pixels"][height-((i//width)+1) + height*(i%width)]=image["pixels"][i]
        elif direction == "left":
            img["pixels"][i//width+height*(width-((i%width)+1))]=image["pixels"][i]
    return img

def shift(image: dict, horizontal: int = 0, vertical: int = 0) -> dict:
    width, height = size(image)
    img = create_blank(width+horizontal,height+vertical)
    for i in range(0, width * height):
        img["pixels"][(i%width)+horizontal+(width+horizontal)*((i//width)+vertical)]=image["pixels"][i]
    return img

def crop(image: dict, x: int, y: int, width: int, height: int) -> dict:
    width1, height1 = size(image)
    img = create_blank(width, height)
    for i in range(0, width * height):
        img["pixels"][i]=image["pixels"][(i%width)+x + width1*((i//width)+y)]
    return img
def filter(image: dict, r: float, g: float, b: float) -> dict:
    width, height = size(image)
    img = create_blank(width, height)
    for i in range(0, width * height):
        r1,g1,b1= int(image["pixels"][i][0]*r),int(image["pixels"][i][1]*g),int(image["pixels"][i][2]*b)

        if r1 > 255:
            r1=255
        if g1 > 255:
            g1=255
        if b1 > 255:
            b1=255
        img["pixels"][i]=(r1,g1,b1)
    return img
def sepia(image):
    width, height=size(image)
    img=create_blank(width,height)
    for i in range(0, width*height):
        r,g,b=image["pixels"][i]
        r,g,b=0.393*r+0.769*g+0.189*b,0.349*r+0.686*g+0.168*b,0.272*r+0.534*g+0.131*b
        if r >255:
            r=255
        if g > 255:
            g = 255
        if b > 255:
            b = 255
        img["pixels"][i]=int(r),int(g),int(b)
    return img

def luminity(image:dict,indice:int)->dict:
    width, height = size(image)
    img = create_blank(width, height)
    for i in range(0, width * height):
        r, g, b = image["pixels"][i]
        r,g,b=r+indice,g+indice,b+indice
        if r > 255:
            r = 255
        if g > 255:
            g = 255
        if b > 255:
            b = 255
        if r < 0:
            r = 0
        if g < 0:
            g = 0
        if b < 0:
            b = 0
        img["pixels"][i] = r, g, b
    return img
def negative(image:dict)->dict:
    width, height = size(image)
    img = create_blank(width, height)
    for i in range(0,width*height):
        r, g, b = image["pixels"][i]
        r,g,b=255-r,255-g,255-b
        img["pixels"][i] = r, g, b
    return img
